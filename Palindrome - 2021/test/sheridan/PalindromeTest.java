package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		
		boolean isPalindrome = Palindrome.isPalindrome("repaper");
		
		assertTrue( "Unable to validate palindrome.", isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Mert");
		
		assertFalse( "Unable to validate palindrome.", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Re paper");
		assertTrue( "Unable to validate palindrome.", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race a car");
		assertFalse( "Unable to validate palindrome.", isPalindrome);
	}	
	
}
